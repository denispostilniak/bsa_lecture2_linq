﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using LINQ.Client.Models;
using LINQ.Client.Models.StructureModels;


namespace LINQ.Client.Interfaces
{
    interface IClientLogicService
    {
        Dictionary<string, int> GetTasksByUser(int id);
        IEnumerable<Models.Task> GetTasksByUserNameCondition(int id);
        IEnumerable<Models.Task> GetTasksByUserCurrentYear(int id);
        IEnumerable<(int, string, List<User>)> GetTeamsOrderTenYears();
        IEnumerable<User> GetUsersSortedByNameAndTasks();
        LastProjectCountAndLongerTasks GetLastProjectCountAndLongerTasks(int userId);
        IEnumerable<ProjectLongestAndShortestTaskAndUsersAmount> GetProjectLogestAndShortestTaskAndUsersAmount();
    }
}
