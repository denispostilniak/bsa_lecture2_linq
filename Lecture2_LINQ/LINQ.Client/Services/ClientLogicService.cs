﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LINQ.Client.Interfaces;
using LINQ.Client.Models;
using LINQ.Client.Models.StructureModels;
using LINQ.Client.Settings;
using Newtonsoft.Json;

namespace LINQ.Client.Services
{
    public class ClientLogicService : IClientLogicService
    {
        private  IEnumerable<Models.Task> _tasks;
        private  IEnumerable<Project> _projects;
        private  IEnumerable<Team> _teams;
        private  IEnumerable<User> _users;

        public ClientLogicService() {
            InitializeLists();
        }

        private async void InitializeLists() {
            var httpService = new HttpService();
            _tasks = await httpService.GetEntitiesEnumerableForType<Models.Task>() ?? new List<Models.Task>();
            _projects = await httpService.GetEntitiesEnumerableForType<Project>() ?? new List<Project>();
            _teams = await httpService.GetEntitiesEnumerableForType<Team>() ?? new List<Team>();
            _users = await httpService.GetEntitiesEnumerableForType<User>() ?? new List<User>();
        }

        public Dictionary<string, int> GetTasksByUser(int id) {
            var userProjects = _projects
                                .Where(project => project.AuthorId == id);
            return userProjects.GroupJoin(_tasks,
                            project => project.Id,
                            task => task.ProjectId,
                            (project, tasks) => (project.Name, tasks.Count()))
                    .ToDictionary(key => key.Name, value => value.Item2);
        }

        public IEnumerable<Models.Task> GetTasksByUserCurrentYear(int id) {
            const int maxNameLenght = 45;

            return _tasks.Where(task => task.PerfomerId == id && task.Name.Length < maxNameLenght);
        }

        public IEnumerable<Models.Task> GetTasksByUserNameCondition(int id) {
                return _tasks.Where(task => task.PerfomerId == id && task.FinishedAt.Year == DateTime.Now.Year);
        }

        public IEnumerable<(int, string, List<User>)> GetTeamsOrderTenYears() {
            const int olderTen = 2010;

            return _teams.GroupJoin(_users,
                            team => team.Id,
                            user => user.TeamId,
                            (team, users) => (team.Id, team.Name, users
                                                .Where(user => user.Birthday.Year < olderTen)
                                                .OrderByDescending(user => user.RegisteredAt)
                                                .ToList()
                                                ));
        }

        public IEnumerable<User> GetUsersSortedByNameAndTasks() {
                return _users.OrderBy(user => user.FirstName).GroupJoin(_tasks,
                                user => user.Id,
                                task => task.PerfomerId,
                                (user, tasks) => { user.Tasks = tasks.OrderByDescending(task => task.Name.Length);
                                    return user;
                                });
        }

        public LastProjectCountAndLongerTasks GetLastProjectCountAndLongerTasks(int userId) {
            var lastProjectStruct = new LastProjectCountAndLongerTasks();

            var res = _users.Where(user => user.Id == userId)
                            .GroupJoin(_projects,
                                        user => user.Id,
                                        project => project.AuthorId,
                                        (user, projects) => {
                                            lastProjectStruct.User = user;
                                            lastProjectStruct.LastProject = projects.OrderByDescending(project => project.CreatedAt)
                                                                                        .FirstOrDefault();
                                            lastProjectStruct.CountTasksPerLastProject = projects.Where(project => project.Id == lastProjectStruct.LastProject.Id)
                                                                                                    .GroupJoin(_tasks,
                                                                                                                project => project.Id,
                                                                                                                task => task.ProjectId,
                                                                                                                (project, tasks) => { return tasks.Count(); }).FirstOrDefault();
                                            return user;
                                        }).GroupJoin(_tasks,
                                                        user => user.Id,
                                                        task => task.PerfomerId,
                                                        (user, tasks) => { lastProjectStruct.NotFinishedOrCanceledTasks = tasks
                                                            .Where(task => task.State == TaskState.Completed || task.State == TaskState.Canceled).Count();
                                                            lastProjectStruct.LongestTask = tasks.OrderByDescending(task => task.FinishedAt - task.CreatedAt)
                                                                                                   .FirstOrDefault();
                                                            return lastProjectStruct;
                                                        }).FirstOrDefault();
            return res;
        }

        public IEnumerable<ProjectLongestAndShortestTaskAndUsersAmount> GetProjectLogestAndShortestTaskAndUsersAmount() {
            var currentProjectLongest = new ProjectLongestAndShortestTaskAndUsersAmount(); 
            
            var res = _projects.GroupJoin(_tasks,
                                            project => project.Id,
                                            task => task.ProjectId,
                                            (project, tasks) => { currentProjectLongest.Project = project;
                                                currentProjectLongest.LongestTaskByDescription = tasks.OrderByDescending(task => task.Description.Length)
                                                                                                        .FirstOrDefault();
                                                currentProjectLongest.ShortestTaskByName = tasks.OrderBy(task => task.Name.Length)
                                                                                                    .FirstOrDefault();
                                                currentProjectLongest.UsersAmountFilteredByProjectProperties =(project.Decription.Length > 20 || tasks.Count() < 3) ? _teams.Where(team => team.Id == project.TeamId)
                                                                                                                        .GroupJoin(_users,
                                                                                                                                    team => team.Id,
                                                                                                                                    user => user.TeamId,
                                                                                                                                    (team, users) => { return users.Count(); }).FirstOrDefault() : 0;
                                                return currentProjectLongest;
                                            });
            return res;
        }
    }
}
