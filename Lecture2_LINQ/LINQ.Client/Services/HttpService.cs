﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LINQ.Client.Models;
using LINQ.Client.Settings;
using Newtonsoft.Json;

namespace LINQ.Client.Services
{
    public class HttpService
    {
        private readonly HttpClient _client;

        public HttpService() {
            _client = new HttpClient { BaseAddress = new Uri(HttpSettings.Host + HttpSettings.ApiEndpoint) };
        }

        public async Task<IEnumerable<T>> GetEntitiesEnumerableForType<T>() {
            try {
                var content = await GetContent(typeof(T));

                return JsonConvert.DeserializeObject<IEnumerable<T>>(await content.ReadAsStringAsync());
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        private async Task<HttpContent> GetContent(Type type) {
            string url = "";
            if (type == typeof(Project)) {
                url = HttpSettings.ProjectsEndpoint;
            }

            if (type == typeof(Team)) {
                url = HttpSettings.TeamsEndpoint;
            }

            if (type == typeof(User)) {
                url = HttpSettings.UsersEndpoint;
            }

            if (type == typeof(Models.Task)) {
                url = HttpSettings.TasksEndpoint;
            }
            var response = await _client.GetAsync(url);

            if (!response.IsSuccessStatusCode) { 
                var message = await response.Content.ReadAsStringAsync();

                throw new Exception(response.StatusCode + "\n" + message);
            }

            return response.Content;
        }
    }
}
