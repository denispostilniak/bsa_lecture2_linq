﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Client.Settings
{
    public static class HttpSettings
    {
        public static string Host { get; } = "https://bsa20.azurewebsites.net/";
        public static string ApiEndpoint { get; } = "api/";

        public static string ProjectsByIdEndpoint { get; } = "Projects/";
        public static string ProjectsEndpoint { get; } = "Projects";

        public static string TasksByIdEndpoint { get; } = "Tasks/";
        public static string TasksEndpoint { get; } = "Tasks";


        public static string TaskStatesEndpoint { get; } = "TaskStates";

        public static string TeamsByIdEndpoint { get; } = "Teams/";
        public static string TeamsEndpoint { get; } = "Teams";

        public static string UsersByIdEndpoint { get; } = "Users/";
        public static string UsersEndpoint { get; } = "Users";
    }
}
